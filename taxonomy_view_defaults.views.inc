<?php
// $Id$

/**
 * @file
 * Provides a view filter which uses a user's default taxonomy terms
 *
 */

 /**
 * Implementation of hook_views_data().
 */
function taxonomy_view_defaults_views_data() {
  $data['taxonomy_view_defaults']['table']['group']  = t('Taxonomy defaults');

  //Join to nodes base through term_node
  $data['taxonomy_view_defaults']['table']['join']['node'] = array(
    'left_table' => 'term_node',
    'left_field' => 'tid',
    'field' => 'tid',
  );
  //Join to terms base directly (may be unneeded)
/*   $data['taxonomy_view_defaults']['table']['join']['term_data'] = array(
    'left_field' => 'tid',
    'field' => 'tid',
  ); */
  //Join to users base directly (may be unneeded)
/*   $data['taxonomy_view_defaults']['table']['join']['users'] = array(
   'left_field' => 'uid',
   'field' => 'uid',
  ); */

  //Filter by User's default term
  /*$data['taxonomy_view_defaults']['tid'] = array(
    'title' => t('User\'s term'),
    'help' => t('Filter by a user\'s default term in a selected vocabulary.'),
    'filter' => array(
      'field' => 'tid',
      'handler' => 'taxonomy_view_defaults_handler_filter_user_default',
    ),
  );*/

  return $data;
}

/**
 * Implementation of hook_views_data_alter().
 */
function taxonomy_view_defaults_views_data_alter(&$data) {
  // tid field
  $data['term_node']['tid_views_default'] = array(
    'help' => t('The taxonomy term ID - User default'),
    'filter' => array(
      'title' => t('Term default'),
      'field' => 'tid',
      'handler' => 'taxonomy_view_defaults_handler_filter_user_default',
      'hierarchy table' => 'term_hierarchy',
      'numeric' => TRUE,
      'skip base' => 'term_data',
    ),
  );
}

/**
 * Implementation of hook_views_handlers()
 */
function taxonomy_view_defaults_views_handlers() {
  $handlers = array(
     'info' => array(
      'path' => drupal_get_path('module', 'taxonomy_view_defaults') .'/includes',
    ),
    'handlers' => array(
      // filter handlers
      'taxonomy_view_defaults_handler_filter_user_default' => array(
          'parent' => 'views_handler_filter_equality',
      ),
    ),
  );
  return $handlers;
}

/**
 * Implementation of hook_views_plugins()
 */
function taxonomy_view_defaults_views_plugins() {
  $path = drupal_get_path('module', 'taxonomy_view_defaults') .'/includes';
  return array(
    'cache' => array(
      'time_per_term' =>array(
        'title' => t('Time-based Per Term'),
        'help' => t('Time-based caching of views on a per term basis. This is necessary if you want to cache a view that filters on "user default term".'),
        'handler' => 'taxonomy_view_defaults_plugin_cache_time',
        'uses options' => TRUE,
        'path' => $path,
        'parent' => 'time',
      ),
    )
  );
}
