<?php
// $Id$

/**
 * @file
 * Interface between taxonomy_view_defaults.module and views.module.
 *
 */

/**
 * Filter nodes to only nodes with the user's default term.
 */
class taxonomy_view_defaults_handler_filter_user_default extends views_handler_filter_equality {
  /*
   * Overload of value_form().
   *
   * Builds the settings form.
   */
  function value_form(&$form, &$form_state) {
    //based on a bit of views_handler_filter_term_node_tid code
    $vocabularies = taxonomy_get_vocabularies();
    foreach ($vocabularies as $voc) {
      //@todo: if enabled to display in a block
      $options[$voc->vid] = check_plain($voc->name);
    }
    $form['value'] = array(
      '#prefix' => '<div class="views-left-40">',
      '#suffix' => '</div>',
      '#type' => 'radios',
      '#title' => t('Vocabulary'),
      '#options' => $options,
      '#description' => t('Select vocabulary to filter by.'),
      '#default_value' => $this->options['value'],
    );

  }

  /*
   * Overload of admin_summary().
   *
   * Returns what to display in the admin summary area
   */
  function admin_summary() {
    $vocabulary = taxonomy_vocabulary_load($this->options['value']);
    if ($this->operator == "=") {
      return t('in @name', array('@name' => $vocabulary->name));
    }
    else {
      return t('not in @name', array('@name' => $vocabulary->name));
    }
  }

  /*
   * Overload of query().
   *
   * Adds additional statements to the SQL query
   */
  function query() {
    global $user;
    
    $vid = $this->options['value'];
    //To pass the current selected term to the term based view cache...    
    $user->taxonomy_view_defaults_current = $vid;

    $account = user_load($user->uid); //get all user data  
    $tid = $account->taxonomy_view_defaults[$vid];
    $showall = variable_get('taxonomy_view_defaults_showall_' . $vid, NULL);    
    if ($tid != $showall && !is_null($tid)) {//if user's selection is show all then, don't modify query
      $tree = taxonomy_get_tree($vid); //@todo: Use db query instead?
      foreach ($tree as $value) {
        //don't include the user's term or showall(which may be NULL)
        if ($value->tid != $tid && $value->tid != $showall) {
          $options[] = $value->tid; //add to list
        }
      }
      
      if($options) { //only add the NOT IN if there is more than one term in vocabulary
        $not_in = implode(",", $options);
        $table = $this->ensure_my_table();
        $this->query->add_where($this->options['group'], "$table.tid NOT IN ($not_in)");
      }
    }
  }
}

