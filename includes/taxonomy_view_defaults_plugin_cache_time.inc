<?php
// $Id$

/**
 * @file
 *  Taxonomy View Defaults plugin that caches views on a per term basis. 
 *  This is necessary for views that filter on "user default term". 
 *
 */

/**
 * Cache plugin that provides caching on a per term basis.
 */
class taxonomy_view_defaults_plugin_cache_time extends views_plugin_cache_time {
  function summary_title() {
    // Return a sumary title for the views admin screen (ex. 1 min/1min (Per Term)).
    return format_interval($this->options['results_lifespan'], 1) . '/' . format_interval($this->options['output_lifespan'], 1) . ' (Per Term)';
  }

  function get_results_key() {
    /**
     * Create an md5 hashed key including the current term
     * to use as the cache key for caching the views results.
     */
    global $user;
    
    if (!isset($this->_results_key)) {
      $vid = $user->taxonomy_view_defaults_current;
      $tid = $user->taxonomy_view_defaults[$vid];
      echo $tid;
      $key_data = array(
        'build_info' => $this->view->build_info,
        'roles' => array_keys($user->roles),
        'super-user' => $user->uid == 1, // Special caching for super user.
        'language' => $GLOBALS['language'],
        'term' => $tid, 
      );
      foreach (array('exposed_info', 'page', 'sort', 'order') as $key) {
        if (isset($_GET[$key])) {
          $key_data[$key] = $_GET[$key];
        }
      }
      // Set the results key.
      $this->_results_key = $this->view->name . ':' . $this->display->id . ':results:' . md5(serialize($key_data));
    }

    return $this->_results_key;
  }

  function get_output_key() {
    /**
     * Create an md5 hashed key including the current term
     * to use as the cache key for caching the views output.
     */
    global $user;
    
    if (!isset($this->_output_key)) {
      $vid = $user->taxonomy_view_defaults_current;
      $tid = $user->taxonomy_view_defaults[$vid];
      $key_data = array(
        'result' => $this->view->result,
        'roles' => array_keys($user->roles),
        'super-user' => $user->uid == 1, // Special caching for super user.
        'theme' => $GLOBALS['theme'],
        'language' => $GLOBALS['language'],
        'term' => $tid,
      );

      $this->_output_key = $this->view->name . ':' . $this->display->id . ':output:' . md5(serialize($key_data));
    }

    return $this->_output_key;
  }
}